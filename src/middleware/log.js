const logMiddleware = ({ dispatch, getState }) => next => action => {
  console.log(`Got: ${ action.type }`, action);

  next(action);
};

export default logMiddleware;