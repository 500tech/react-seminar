import { getID } from '../lib/ids';

const apiMiddleware = ({ dispatch, getState }) => next => action => {

  const payload = [
    {
      id: getID(),
      title: 'Waffles',
      description: 'Nice',
      favorite: false
    },
    {
      id: getID(),
      title: 'Omelette',
      description: 'Good',
      favorite: true
    },
    {
      id: getID(),
      title: 'Dog Food',
      description: 'Amazing',
      favorite: true
    }
  ];

  if (action.type === 'API') {
    setTimeout(() => {
      dispatch({ type: action.payload.success, payload })
    }, 3000)
  } else {
    next(action);
  }
};

export default apiMiddleware;