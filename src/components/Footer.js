import React from 'react';
import Counter from './Counter';

const Footer = () => (
  <footer>
    The Recipes Book Project (<Counter favorite={ false } />)
  </footer>
);

export default Footer;