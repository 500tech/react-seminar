import * as actions from '../consts/action-types';

export const addRecipe = (title, description, favorite = false) => ({
  type: actions.ADD_RECIPE,
  title: title.trim(),
  description: description.trim(),
  favorite
});

export const toggleFavorite = (id) => ({
  type: actions.TOGGLE_FAVORITE,
  payload: id,
  meta: {
    ga: 'TOGGLE_FAVORITE',
    debounce: 100
  }
});

export const fetchRecipes = () => ({
  type: 'API',
  payload: {
    url: 'recipes.json',
    method: 'GET',
    success: actions.SET_RECIPES
  }
});

export const renameRecipe = (id, title) => ({
  type: 'API',
  payload: {
    url: `recipe/${ id }`,
    method: 'PUT',
    success: actions.UPDATE_RECIPE,
    data: { title }
  }
});