import recipes from './recipes';
import user from './user';
import ingredients from './ingeridents';
import { combineReducers } from 'redux';

export default combineReducers({
  recipes,
  ingredients,
  user
});
