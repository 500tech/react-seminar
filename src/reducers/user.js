const initialState = {
  name: 'Kipi'
};

const userReducer = (user = initialState, action) => {

  switch (action.type) {
    case 'SET_USER':
      return Object.assign({}, user, {
        name: action.name
      });

    default:
      return user;
  }
};

export default userReducer;