import * as actions from '../consts/action-types';
import { getID } from '../lib/ids';

const initialState = [];

const reducer = (recipes = initialState, action) => {
  console.log("Got Action " + action.type, action);

  switch (action.type) {
    case actions.ADD_RECIPE:
      return recipes.concat({
        id: getID(),
        title: action.title,
        favorite: false,
        description: action.description
      });

    case actions.TOGGLE_FAVORITE:
       return recipes.map(recipe =>
        recipe.id !== action.id
          ? recipe
          : Object.assign({}, recipe, {
            favorite: !recipe.favorite
          })
      );

    case actions.SET_RECIPES:
      return action.payload;

    default:
      return recipes;
  }
};

export default reducer;